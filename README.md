# CVaR

Implementations of different methods for minimizing CVaR problem for a discrete distribution applied in shortest path problem and portfolio optimisation, and comparison of these methods.

## Data

Real data files for the studying problem, for testing.

## Rapport

Report, in French.

## Report

English version of the report.

## Result

Mainly .txt files that contains the output of some execution of test functions, and .csv with the results from the text files.

## Source

Julia files, contains the code (implementation of the problems, the methods, and testing functions).
