"""Functions that return the (lower) minCVaR by getting the models to calculate it and launch them."""


include("Model/default.jl")
include("Model/stochastic.jl")
include("Model/alphabeta.jl")
include("Model/minmax.jl")


function calculate_cvar(cvar_model, alpha, Y, relaxation=false)
	"""Calculate the minCVaR using the method "cvar_model"."""

	model, x = cvar_model(alpha, Y)

	if relaxation
		undo_relax = relax_integrality(model)
	end

	optimize!(model)
	cvar = objective_value(model)

	return cvar
end


function calculate_cvar_stochastically(cvar_stochastic_model, alpha, Y, print_number_of_iterations=false, relaxation=false)
	"""Calculate the minCVaR using the method "cvar_stochastic_model"."""

	final_model, number_of_iterations, constraints = cvar_stochastic_model(alpha, Y, relaxation)

	if print_number_of_iterations
		println("Found after ", number_of_iterations, " iterations.")
	end

	cvar = objective_value(final_model)
	return cvar
end






function cvar_default(alpha, Y; relaxation=false)
	"""Function that calculate the minCVaR using the (default) formulation."""
	
	return calculate_cvar(cvar_default_model, alpha, Y, relaxation)
end


function cvar_alphabeta(alpha, Y; relaxation=false)
	"""Return the minCVaR using the (alpha/beta) algorithm."""

	return calculate_cvar(cvar_alphabeta_model, alpha, Y, relaxation)
end



function cvar_stochastic(alpha, Y; print_number_of_iterations=false, relaxation=false)
	"""Return the minCVaR using the (stoch)astic algorithm."""

	return calculate_cvar_stochastically(cvar_stochastic_final_model, alpha, Y, print_number_of_iterations, relaxation)
end


function cvar_minmax(alpha, Y; print_number_of_iterations=false, relaxation=false)
	"""Return the minCVaR using the (min/max) algorithm."""

	return calculate_cvar_stochastically(cvar_minmax_final_model, alpha, Y, print_number_of_iterations, relaxation)
end










function lowercvar_alphabeta(alpha, Y; relaxation=false)
	"""Return the min(lower CVaR) using an the (alpha/beta) algortihm."""

	return calculate_cvar(lowercvar_alphabeta_model, alpha, Y, relaxation)
end




function lowercvar_minmax(alpha, Y; print_number_of_iterations=false, relaxation=false)
	"""Return the min(lower CVaR) using the (min/max) algorithm."""

	return calculate_cvar_stochastically(lowercvar_minmax_final_model, alpha, Y, print_number_of_iterations, relaxation)
end
