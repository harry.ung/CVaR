"""Generic testing functions, that apply a function test to a set of data."""

include("cvar.jl")
include("graph.jl")
include("asset.jl")



function minitest(test; alpha=0.95, Y=minigraph())
	test(alpha, Y)
end


function multiple_portfolio_test(test, alphas, Nobs)
	"""Test multiple alpha and number_of_observations with a portfolio problem."""

	for number_of_observations in Nobs
	   for alpha in alphas
			println("===alpha : ", alpha, " number of observations : ", number_of_observations, "===")
			minitest(test, alpha=alpha, Y=miniasset(number_of_observations))
	   end
   end
end


function multiple_generated_portfolio_test(test, number_of_assets, alphas, Nobs)
	"""Test multiple alpha and number_of_observations with "number_of_assets" generated assets."""

	for number_of_observations in Nobs
	   for alpha in alphas
			println("===alpha : ", alpha, " number of observations : ", number_of_observations, "===")
			println("# Number of assets : ", number_of_assets)
			test(alpha, generate_assets(number_of_assets, number_of_observations))
	   end
   end
end


function quicktest(test, alpha = 0.5, number_of_vertices = 25, number_of_observations = 25)
	"""Apply the function "test" on the problem of the sortest path problem.
	The test is made on a strongly connected graph with random costs."""

	graph = generate_strongly_connected_graph(number_of_vertices, number_of_observations)

	test(alpha, graph)	
end



function test_on_file(test, file, alpha, number_of_observations)
	"""Do a test with the graph of 'file'."""

	graph = get_graph_from_file(file, number_of_observations)	

	print(file, "\n")
	test(alpha, graph)
	print("\n")
end


function multiple_test_on_file(test, file, alphas=[0.05*k for k in 1:19], Nobs=[5*k for k in 1:20])
	"""Test multiple alpha and number_of_observations with the graph 'file'."""

	for number_of_observations in Nobs
	   for alpha in alphas
			println("===alpha : ", alpha, " number of observations : ", number_of_observations, "===")
			test_on_file(test, file, alpha, number_of_observations)
	   end
   end
end


function test_on_all_files(test, alpha, number_of_observations, path="../Data/Data/")
	"""Do a test on all files in path."""

	println("===alpha : ", alpha, " number of observations : ", number_of_observations, "===")

	for filename in readdir(path)
		test_on_file(test, path*filename, alpha, number_of_observations)
	end
end




function test_on_all_files_and_save_result(test, alpha, number_of_observations, path_to_file, path_to_savefile)
	"""Launch test_on_all_files(alpha, number_of_observations, path_to_file)
	and redirect the output on a file named with the parameters."""

	filename = string(path_to_savefile, "alpha_", Int(100*alpha), "_nobs_", number_of_observations, ".txt")

	minitest(test) #Mock test, which is here to ignore the compilation time.

	open(filename, "w") do file
		redirect_stdout(file) do
			test_on_all_files(test, alpha, number_of_observations, path_to_file)
		end
	end
end



function multiple_test_on_file_and_save_result(test, input_file, output_file, alphas=[0.05*k for k in 1:19], Nobs=[5*k for k in 1:20])
	"""Launch multiple_test_on_file(input_file) and redirect the output on the file output_file."""

	minitest(test) #Mock test, which is here to ignore the compilation time.

	open(output_file, "w") do file
		redirect_stdout(file) do
			multiple_test_on_file(test, input_file, alphas, Nobs)
		end
	end
end






function multiple_portfolio_test_and_save_result(test, output_file, alphas, Nobs)

	minitest(test) #Mock test, which is here to ignore the compilation time.

	open(output_file, "w") do file
		redirect_stdout(file) do
			multiple_portfolio_test(test, alphas, Nobs)
		end
	end
end

function multiple_generated_portfolio_and_save_result(test, output_file, number_of_assets, alphas, Nobs)

	minitest(test) #Mock test, which is here to ignore the compilation time.

	open(output_file, "w") do file
		redirect_stdout(file) do
			multiple_generated_portfolio_test(test, number_of_assets, alphas, Nobs)
		end
	end
end
