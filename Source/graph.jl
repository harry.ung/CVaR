"""Set of functions to generate graph."""

include("Model/shortest_path.jl")

function minigraph()
	
	Cost = [[10, 2, 25, 3, 4], [11, 3, 28, 5, 2]]
	Edge = [[1, 2], [1, 3], [2, 4], [3, 5], [4, 5]]
	number_of_vertices = 5

	return Graph(Cost, Edge, number_of_vertices)
end

function generate_strongly_connected_graph(number_of_vertices, number_of_observations)
	
	Edge = [[i, j] for i in 1:number_of_vertices for j in 1:number_of_vertices]
	Cost = [50*rand(length(Edge)) for k in 1:number_of_observations]

	return Graph(Cost, Edge, number_of_vertices)
end


function rand_disturb(n)
	"""Create a vector of n numbers following a exponential distribution of parameter 1."""

	return -map(log, rand(n))
end

function disturb_cost(cost, number_of_observations)
	"""Create observations by disturbing the original cost."""

	number_of_edges = length(original_cost)
	Cost = [original_cost]
	for obs in 2:number_of_observations
		push!(Cost, Cost[1] .* rand_disturb(number_of_edges) )
	end

	return Cost
end

function get_graph_from_file(file, number_of_observations)
	
	#File already has Edge, number_of_vertices, and an original_cost for each edge.
	include(file)

	Cost = disturb_cost(original_cost, number_of_observations)

	return Graph(Cost, Edge, number_of_vertices)
end
