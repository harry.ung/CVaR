using JuMP
using LinearAlgebra
import Permutations

include("owa.jl")
include("model.jl")



function alphabeta_model(alpha_, betacoeff, initlam, Y)
	"""Return (alpha/beta) model which minimize OWA(x) for a Y problem with a lambda given with the function initlam."""

	model = create_model()
	x = add_constraints_on_x!(model, Y)

	lam = initlam(alpha_, Y.number_of_observations)

	@variable(model, alpha[1:Y.number_of_observations])
	@variable(model, beta[keys(betacoeff)])

	@objective(model, Min,
		sum(alpha) + sum(betacoeff[i] * beta[i] for i in eachindex(betacoeff))
	)
	
	@constraint(model, constraint_on_alpha_beta[i in 1:Y.number_of_observations, j in eachindex(betacoeff)],
		alpha[i] + beta[j] >= lam[j] * f(i, x, Y)
	)
	
	return model, x
end



function cvar_alphabeta_model(alpha_, Y)
	"""Return the (alpha/beta) model to minimize CVaR."""

	n = Y.number_of_observations
	p = getp(alpha_, n)

	# Warning : the order of definition is important to avoid problem when p ==1 or p == n.
	betacoeff = Dict(1 => p-1, n => n-p, p => 1)
	model, x = alphabeta_model(alpha_, betacoeff, initcvarlam, Y)

	return model, x
end



function lowercvar_alphabeta_model(alpha_, Y)
	"""Return the (alpha/beta) model to minimize lower CVaR."""

	n = Y.number_of_observations
	p = getp(alpha_, n)

	betacoeff = Dict(1 => p-1, n => n-p+1)
	model, x = alphabeta_model(alpha_, betacoeff, initlowerlam, Y)


	return model, x
end


