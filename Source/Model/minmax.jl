using JuMP
using LinearAlgebra
import Permutations

include("owa.jl")
include("model.jl")


function sortlam(alpha, y, initlam)
	"""Sort lambda to have the same order as y."""

	new_index_order = inv(Permutations.Permutation(sortperm(y))).data

	number_of_observations = length(y)	
	new_lam = initlam(alpha, number_of_observations)[new_index_order]
	
	return new_lam
end



function minmax_model(alpha, lambdas, Y)
	"""Create the (min/max) model for a given set of lambda to calculate the CVaR on the problem Y."""

	model = create_model()
	x = add_constraints_on_x!(model, Y)

	@variable(model, z >= 0)

	@objective(model, Min, z)
	
	@constraint(model, constraint_on_lambdas[lam in 1:length(lambdas)],
		z >= sum(lambdas[lam][i] * f(i, x, Y) for i in 1:Y.number_of_observations)
	)

	return model, x 
end


function minimization(model, x)
	"""Minimize OWA(x) and return x_optimal."""

	optimize!(model)

	x_opt = value.(x)

	return x_opt
end

function maximization(alpha, initlam, x, Y)
	"""Maximize dot(lambda, x) and return lam_optimal."""

	lam_opt = sortlam(alpha, f(x, Y), initlam)	

	return lam_opt
end



function minmax_final_model(alpha, initlam, Y, relaxation=false)
	"""Return the final model of the (min/max) algorithm."""

	model, number_of_iterations = Model(), 0
	lambdas, lambda_optimal = [], initlam(alpha, Y.number_of_observations)
	while !(lambda_optimal in lambdas)
		number_of_iterations += 1

		push!(lambdas, lambda_optimal)
		
		model, x = minmax_model(alpha, lambdas, Y)
		if relaxation
			relax_integrality(model)
		end
		x_optimal = minimization(model, x)

		lambda_optimal = maximization(alpha, initlam, x_optimal, Y)
	end

	return model, number_of_iterations, lambdas
end


function cvar_minmax_final_model(alpha, Y, relaxation=false)#, lambdas=[initcvarlam(alpha, Y.number_of_observations)])
	"""Calculate minCVaR using the (min/max) algorithm and return the final model."""
	
	return minmax_final_model(alpha, initcvarlam, Y, relaxation)
end



function lowercvar_minmax_final_model(alpha, Y, relaxation=false)#, lambdas=[initlowerlam(alpha, Y.number_of_observations)])
	"""Calculate min(lower CVaR) using the (min/max) algorithm and return the final model."""
	
	return minmax_final_model(alpha, initlowerlam, Y, relaxation)
end
