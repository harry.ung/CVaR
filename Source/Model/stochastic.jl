using JuMP
using LinearAlgebra

include("model.jl")


function cvar_stochastic_model(alpha, Y, partitions)
	"""Create the "min/max" model for a given set of lambda to calculate the CVaR on a shortest path problem."""

	model = create_model()
	x = add_constraints_on_x!(model, Y)

	@variable(model, K)
	@variable(model, z >= 0)

	@objective(model, Min, K + z / (1-alpha))
	
	@constraint(model, constraint_on_z[part in 1:length(partitions)],
		z >= sum( f(i, x, Y) - K  for i in partitions[part] ) / Y.number_of_observations
	)

	return model, x, K 
end


function solve_stochastic_model(model, x, K)
	"""Solve model and return the optimal values."""

	optimize!(model)

	x_opt = value.(x)
	K_opt = value.(K)

	return x_opt, K_opt
end

function check_for_optimality(alpha, x, Y, K)
	"""Check for a possible new cut."""
	
	set_opt = [i for i in 1:Y.number_of_observations if (f(i, x, Y) - K) > 0.]

	return set_opt
end



function cvar_stochastic_final_model(alpha, Y, relaxation=false) #, partitions=[ Vector(1:Y.number_of_observations) ])
	"""Applied the stochastic algorithm of Mayer to minimize CVaR and return the final model, already optimzed."""
	
	model, number_of_iterations = Model(), 0
	partitions, part_optimal = [], Vector(1:Y.number_of_observations)
	while !(part_optimal in partitions) && (part_optimal != [])
		number_of_iterations += 1

		push!(partitions, part_optimal)

		model, x, K = cvar_stochastic_model(alpha, Y, partitions)
		if relaxation
			relax_integrality(model)
		end
		x_optimal, K_optimal  = solve_stochastic_model(model, x, K)
		
		part_optimal = check_for_optimality(alpha, x_optimal, Y, K_optimal)
	end

	return model, number_of_iterations, partitions
end
