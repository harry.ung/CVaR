"""Set of funtions that are useful for all model to calculate CVaR."""

using JuMP
using GLPK, CPLEX
using LinearAlgebra

include("portfolio.jl")
include("shortest_path.jl")


SOLVER = CPLEX

const EPSILON = 10^-10


function create_model()
	"""Function that create a model with default parameter."""
	
	model = Model(SOLVER.Optimizer)
	set_silent(model)

	return model
end


function f(x, Y)
	"""Return the vector of f(i, x, Y)."""

	fx = [f(i, x, Y) for i in 1:Y.number_of_observations]

	return fx
end
