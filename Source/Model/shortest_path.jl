using JuMP
using LinearAlgebra


struct Graph{Float<:Real, Int<:Integer}
	Cost::Vector{Vector{Float}}
	Edge::Vector{Vector{Int}}
	number_of_edges::Int
	number_of_observations::Int
	number_of_vertices::Int
	source_node::Int
	sink_node::Int
end

Graph(Cost, Edge, number_of_vertices, source_node, sink_node) = Graph(Cost, Edge, length(Edge), length(Cost), number_of_vertices, source_node, sink_node)


Graph(Cost, Edge, number_of_vertices) = Graph(Cost, Edge, number_of_vertices, 1, number_of_vertices)






function init_node(edge)
	"""Function that return the initial node of an edge."""

	return edge[1]
end

function term_node(edge)
	"""Function that return the terminal node of an edge."""

	return edge[2]
end

function add_constraints_on_x!(model::Model, graph::Graph)
	"""Function that build a model with the constraints of a shortest path problem."""

	@variable(model, x[1:graph.number_of_edges], Bin)
	
	@constraint(model, constraint_on_source_node,
		1 - sum(x[i] for i in 1:graph.number_of_edges if init_node(graph.Edge[i]) == graph.source_node)
		== 0
	)
	@constraint(model, constraint_on_node[node in 2:(graph.number_of_vertices-1)],
		sum(x[i] for i in 1:graph.number_of_edges if init_node(graph.Edge[i]) == node)
		- sum(x[j] for j in 1:graph.number_of_edges if term_node(graph.Edge[j]) == node)
		== 0
	)
	@constraint(model, constraint_on_sink_node,
		sum(x[j] for j in 1:graph.number_of_edges if term_node(graph.Edge[j]) == graph.sink_node) - 1
		== 0
	)

	return x
end



function f(i::Integer, x, Y::Graph)
	"""Cost function of the portfolio problem."""

	return dot(x, Y.Cost[i])
end
