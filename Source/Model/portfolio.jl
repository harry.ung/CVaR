using JuMP
using LinearAlgebra


struct Assets{Float<:Real, Int<:Integer}
	Return::Vector{Vector{Float}}
	minimum_expected_return::Float
	number_of_assets::Int
	number_of_observations::Int
end

function get_number_of_assets(Return)
	first_observation = Return[1]

	return length(first_observation)
end

function get_number_of_observations(Return)
	return length(Return)
end

Assets(Return, minimum_expected_return) = Assets(Return, minimum_expected_return, get_number_of_assets(Return), get_number_of_observations(Return))






function mean(X)
	return sum(X) ./ length(X)
end

function add_constraints_on_x!(model::Model, assets::Assets)
	"""Function that build a model with the constraints of a portfolio optimization problem."""
	
	@variable(model, x[1:assets.number_of_assets] >= 0.)
	

	expected_return = mean(assets.Return)

	@constraint(model, constraint_on_minimal_return,
		dot(expected_return, x) >= assets.minimum_expected_return
	)
	@constraint(model, constraint_on_all_x, sum(x) == 1.)

	return x
end



function f(i::Integer, x, Y::Assets)
	"""Cost function of the portfolio problem."""

	return -dot(x, Y.Return[i])
end
