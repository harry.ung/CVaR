using JuMP
using LinearAlgebra

include("model.jl")


function cvar_default_model(alpha, Y)
	"""Function that build the default formulation of the CVaR-minimizing model of Rockafeller and Uryasef."""

	model = create_model()
	x = add_constraints_on_x!(model, Y)

	@variable(model, K)
	@variable(model, z[1:Y.number_of_observations] >= 0)

	@objective(model, Min, K + sum(z) / (Y.number_of_observations * (1-alpha)) )

	@constraint(model, constraint_on_z[i in 1:Y.number_of_observations],
		z[i] >= f(i, x, Y) - K
	)

	return model, x
end

