"""Set of functions that are useful for all OWA-based method."""

using JuMP
using LinearAlgebra
import Permutations


function getp(alpha, n)
	"""Return the integer p such as p/n >= alpha."""

	return Int(ceil(alpha*n))
end


function initcvarlam(alpha, n)
	"""Initialize the vector lambda to obtain the coefficients to calculate the CVaR."""

	p = getp(alpha, n)

	lam = fill(0., n)
	lam[p] = ((p/n) - alpha) / (1 - alpha)
	lam[(p+1):n] .= 1 / (n*(1-alpha))

	return lam
end


function initlowerlam(alpha, n)
	"""Initialize the vector lambda to obtain the coefficients to calculate the lower CVaR."""

	p = getp(alpha, n)

	lam = fill(0., n)
	lam[p:n] .= 1 / (n - p + 1)

	return lam
end

