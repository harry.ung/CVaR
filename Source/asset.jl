"""Set of functions to generate Assets."""

import Distributions

include("Model/portfolio.jl")


function generate_returns(mu, sigma, number_of_observations)
	"""Generate a sample of number_of_observations returns which follow the law of multivariate normal distribution (mi, sigma)."""

	law = Distributions.MvNormal(mu, sigma)

	Return = rand(law, number_of_observations)
	#Conversion
	Return = [ Return[:,i] for i in 1:size(Return)[2] ]

	return Return
end


function miniasset(number_of_observations)
	
	#S&P 500, Gov Bond, Small Cap
	mu = [0.0101110, 0.0043532, 0.0137058]
	sigma = [0.00324625 0.00022983 0.00420395 ;
	0.00022983 0.00049937 0.00019247 ;
	0.00420395 0.00019247 0.00764097]

	minimum_expected_return = 0.011
	

	Return = generate_returns(mu, sigma)

	#Ensure a feasible solution to the portfolio problem.
	while maximum(mean(Return)) < minimum_expected_return
		Return = generate_returns(mu, sigma)
	end

	return Assets(Return, minimum_expected_return)
end



function generate_assets(number_of_assets, number_of_observations)
	"""Generated a set of assets of number_of_assets, whith number_of_observations returns."""
	
	mu = rand(number_of_assets) / 500.
	#Random positive matrix
	sigma = rand(number_of_assets, number_of_assets) / 500. ; sigma = sigma' * sigma
	
	Return = generate_returns(mu, sigma)	

	minimum_expected_return = mean(mean(Return))
	
	return Assets(Return, minimum_expected_return)
end

