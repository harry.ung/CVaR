include("test.jl")


print(SOLVER, "\n")



function time_test(alpha, Y; relax=false)
	"""Calculate the minCVaR on a Y with different methods, and compare their time execution."""

	println("Execution time of the default formulation method : ")
	@time begin
		default = cvar_default(alpha, Y, relaxation=relax)
	end

	println("Execution time of the stochastic formulation method : ")
	@time begin
		stoch = cvar_stochastic(alpha, Y, print_number_of_iterations=true, relaxation=relax)
	end

	println("Execution time of the OWA method (min/max formulation): ")
	@time begin
		minmax = cvar_minmax(alpha, Y, print_number_of_iterations=true, relaxation=relax)
	end

	println("Execution time of the OWA method (alpha/beta formulation): ")
	@time begin
		alphabeta = cvar_alphabeta(alpha, Y, relaxation=relax)
	end


	println("Default : ", default, "\nStoch : ", stoch, "\nOWA (min/max) : ", minmax, "\nOWA (alpha/beta) : ", alphabeta, "\n")
end



function relax_time_test(alpha, Y)
	"""Calculate the minCVaR on the relaxed Y problem with different methods."""

	return time_test(alpha, Y, relax=true)
end



function relax_test(alpha, Y)
	"""Calculate the minCVaR on a Y problem, and its relaxed problem too."""

	cvar = cvar_minmax(alpha, Y, relaxation=false)
	println("CVaR : ", cvar)

	#=ref = cvar_default(alpha, Y, relaxation=true)
	println("Relax : ", ref)=#

	relax = cvar_stochastic(alpha, Y, relaxation=true)
	println("Relax : ", relax)
end



function lower_test(alpha, Y; relax=false)
	"""Calculate the minCVaR and the min(lower CVaR) on a Y with different methods, and compare their time execution."""

	println("Execution time of the (min/max) method to calculate cvar: ")
	@time begin
		minmax = cvar_minmax(alpha, Y, print_number_of_iterations=true, relaxation=relax)
	end

	println("Execution time of the (alpha/beta) method to calculate cvar: ")
	@time begin
		alphabeta = cvar_alphabeta(alpha, Y, relaxation=relax)
	end

	println("Execution time of the (min/max) method to calculate lower cvar: ")
	@time begin
		lower_minmax = lowercvar_minmax(alpha, Y, print_number_of_iterations=true, relaxation=relax)
	end

	println("Execution time of the (alpha/beta) method to calculate lower cvar: ")
	@time begin
		lower_alphabeta = lowercvar_alphabeta(alpha, Y, relaxation=relax)
	end
	

	println("CVaR (min/max) : ", minmax, "\nCVaR (alpha/beta) : ", alphabeta, "\nlower CVaR (min/max) : ", lower_minmax, "\nlower CVaR (alpha/beta) : ", lower_alphabeta)
end
