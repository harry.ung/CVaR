#!/bin/bash

#Extract the number of nodes, the edges and the costs of data from TransportationNetworks.

function extract_data()
{
	input=$1
	output=$2

	> $output

	#Extract the number of vertices.
	grep "NUMBER OF NODES" $input | sed -e "s/[^1-9]*\([1-9]*\)/number_of_vertices = \1/" >> $output
	#Extract the number of edges.
	grep "NUMBER OF LINKS" $input | sed -e "s/[^1-9]*\([1-9]*\)/number_of_edges = \1/" >> $output

	#Extract the edges of the graph.
	echo "Edge = [" >> $output
	sed -ne 's/[\t ]*\([0-9]\+\)[\t ]\+\([0-9]\+\).*/\[\1, \2\],/p' $input >> $output
	#...replace the comma from the last lines.
	lastline=$(wc -l < $output)
	sed -i -e $(echo "$[lastline]s/\],/\]\n\]/") $output

	#Extract the cost of the graph.
	echo "original_cost = [" >> $output
	sed -ne 's/[\t ]*[0-9]\+[\t ]\+[0-9]\+[\t ]\+[0-9\.e+]\+[\t ]\+\([0-9]*\)\.*\([0-9]*\).*/\1.\2,/p' $input >> $output
	#...replace the comma from the last lines.
	lastline=$(wc -l < $output)
	sed -i -e $(echo "$[lastline]s/,/\n\]/") $output
}

for input in $(find TransportationNetworks-master/ -name "*_net.tntp") ; do
	output=Data/$(sed -e "s/\([^\.]*\)\.tntp/\1\.jl/" <<<$(basename $input))
	extract_data $input $output
done

